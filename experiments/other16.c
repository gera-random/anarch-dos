asm(".code16gcc\n"
	"call	start\n"
    "mov	$0x4C, %ah\n"
    "int	$0x21\n");

#include "common.h"

int start(void)
{
	putc('h');
	putc('i');
	putc('\r');
	putc('\n');
	puts("hello from another program!\n");
	return 0;
}
