# Building

Use [DJGPP cross-compiler](https://github.com/andrewwutw/build-djgpp) or install DJGPP on dos.

```
$ make
$ dosbox-x anarch.exe
```

To build code that uses [SmallerC](https://github.com/alexfru/SmallerC), you
should build the compiler with `-D_MAX_IDENT_TABLE_LEN` set to a high value. I
switched to DJGPP because performance of the program compiled by SmallerC was
too low.

# TODO

- Sound

# Borrowed code

Drawing in unchained mode, page flipping from [VGA programming tutorial](http://www.brackeen.com/vga/unchain.html) ("Please feel free to copy this source code");

*sleepMs, getTimeMs* from [anarch-dos by Wuuff](https://gitlab.com/wuuff/anarch-dos) (public domain);

*vga_on, vga_off, com.ld* from [DOS Defender](https://github.com/skeeto/dosdefender-ld31) (public domain);

Handling keyboard from [Handling Interrupts](https://www.oocities.org/siliconvalley/park/Park/8933/interrupts.html).
