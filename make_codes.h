#pragma once

// https://helppc.netcore2k.net/table/make-codes

#define MAKE_A 0x1E
#define MAKE_B 0x30
#define MAKE_C 0x2E
#define MAKE_D 0x20
#define MAKE_E 0x12
#define MAKE_F 0x21
#define MAKE_G 0x22
#define MAKE_H 0x23
#define MAKE_I 0x17
#define MAKE_J 0x24
#define MAKE_K 0x25
#define MAKE_L 0x26
#define MAKE_M 0x32
#define MAKE_N 0x31
#define MAKE_O 0x18
#define MAKE_P 0x19
#define MAKE_Q 0x10
#define MAKE_R 0x13
#define MAKE_S 0x1F
#define MAKE_T 0x14
#define MAKE_U 0x16
#define MAKE_V 0x2F
#define MAKE_W 0x11
#define MAKE_X 0x2D
#define MAKE_Y 0x15
#define MAKE_Z 0x2C

#define MAKE_BACKSPACE 0x0E
#define MAKE_LSHIFT 0x2A
#define MAKE_RSHIFT 0x36
#define MAKE_CTRL 0x1D
#define MAKE_SPACE 0x39
#define MAKE_TAB 0x0F
#define MAKE_ENTER 0x1C
#define MAKE_ESCAPE 0x01

#define MAKE_ENHANCED 0xE0
#define MAKE_UP 0x48
#define MAKE_DOWN 0x50
#define MAKE_LEFT 0x4B
#define MAKE_RIGHT 0x4D
