#include <stdio.h>
#include <unistd.h>
#include <dos.h>
#include <sys/nearptr.h>
#include <sys/timeb.h>
#include <string.h>

#include "keyboard.h"
#include "make_codes.h"
#define VERTICAL_RETRACE
#include "vga.h"
#define SFG_SCREEN_RESOLUTION_X SCREEN_WIDTH
#define SFG_SCREEN_RESOLUTION_Y SCREEN_HEIGHT
#define SFG_FPS 22
#include "anarch/game.h"

uint32_t timeStart;

uint32_t getTime()
{
  struct timeb now;
  ftime(&now);
  return now.time * 1000 + now.millitm;
}

void getMouseKeyStatus(int8_t *is_left, int8_t *is_right)
{
	union REGS regs;

	regs.w.ax = 0x03;
	int86(0x33, &regs, &regs);

	*is_left = regs.w.bx & 0b1;
	*is_right = regs.w.bx & 0b10;
}

void getMouseOffset(int16_t *dx, int16_t *dy)
{
	union REGS regs;

	regs.w.ax = 0x0B;
	int86(0x33, &regs, &regs);

	*dx = regs.w.cx;
	*dy = regs.w.dx;
}

uint32_t SFG_getTimeMs()
{
  return getTime() - timeStart;
}

void SFG_sleepMs(uint16_t timeMs)
{
	usleep(timeMs * 1000);
}

void SFG_setPixel(uint16_t x, uint16_t y, uint8_t colorIndex)
{
	plot_pixel(x, y, colorIndex);
}

void SFG_processEvent(uint8_t event, uint8_t data) { }
int8_t SFG_keyPressed(uint8_t key) {
	int8_t mouse_left = 0, mouse_right = 0;
	getMouseKeyStatus(&mouse_left, &mouse_right);
	switch(key) {
		case SFG_KEY_TOGGLE_FREELOOK: return mouse_right;
		case SFG_KEY_UP: return key_status[MAKE_W] || (key_status[MAKE_ENHANCED] && key_status[MAKE_UP]);
		case SFG_KEY_DOWN: return key_status[MAKE_S] || (key_status[MAKE_ENHANCED] && key_status[MAKE_DOWN]);
		case SFG_KEY_STRAFE_LEFT: return key_status[MAKE_A] || (key_status[MAKE_ENHANCED] && key_status[MAKE_LEFT]);
		case SFG_KEY_STRAFE_RIGHT: return key_status[MAKE_D] || (key_status[MAKE_ENHANCED] && key_status[MAKE_RIGHT]);
		case SFG_KEY_LEFT: return key_status[MAKE_Q];
		case SFG_KEY_RIGHT: return key_status[MAKE_E];
		case SFG_KEY_A: return mouse_left || key_status[MAKE_J] || key_status[MAKE_CTRL] || key_status[MAKE_ENTER];
		case SFG_KEY_B: return key_status[MAKE_K] || key_status[MAKE_LSHIFT];
		case SFG_KEY_C: return key_status[MAKE_L];
		case SFG_KEY_JUMP: return key_status[MAKE_SPACE];
		case SFG_KEY_CYCLE_WEAPON: return key_status[MAKE_F];
		case SFG_KEY_NEXT_WEAPON: return key_status[MAKE_P] || key_status[MAKE_X];
		case SFG_KEY_MAP: return key_status[MAKE_TAB] || key_status[MAKE_BACKSPACE];
		case SFG_KEY_MENU: return key_status[MAKE_ESCAPE];
	}
	return 0;
}

void SFG_getMouseOffset(int16_t *x, int16_t *y) {
	getMouseOffset(x, y);
}

void SFG_setMusic(uint8_t value) { }
void SFG_playSound(uint8_t soundIndex, uint8_t volume) { }

void SFG_save(uint8_t data[SFG_SAVE_SIZE]) {
	FILE *f = fopen(SFG_SAVE_FILE_PATH, "wb");

	if (f == 0) {
		puts("SFG_save: cannot open save file\n");
	}
	else if (fwrite(data,1,SFG_SAVE_SIZE,f) != SFG_SAVE_SIZE) {
		puts("SFG_save: partial write\n");
	}
	if (fclose(f) == EOF) {
		puts("SFG_save: file is not closed\n");
	}
}

uint8_t SFG_load(uint8_t data[SFG_SAVE_SIZE])
{
	FILE *f = fopen(SFG_SAVE_FILE_PATH, "rb");

	puts("SFG_load: opening and reading save file\n");

	if (f == 0) {
		puts("SFG_load: no save file to open\n");
	}
	else if (fread(data,1,SFG_SAVE_SIZE,f) != SFG_SAVE_SIZE) {
		puts("SFG_load: partial read\n");
	}
	else if (fclose(f) == EOF) {
		puts("SFG_load: file is not closed\n");
	}
	return 1;
}

void fill_rgb666_palette(const int16_t *rgb565_palette)
{
	palette_start_input();
	for (int i = 0; i < 256; ++i)
	{
		int8_t col = (rgb565_palette[i] >> 11) & 0x1f;
		col = col * 63 / 31;

		palette_add_color(col);

		col = (rgb565_palette[i] >> 5) & 0x3f;

		palette_add_color(col);

		col = rgb565_palette[i] & 0x1f;
		col = col * 63 / 31;

		palette_add_color(col);
	}
}

int main(int argc, char **argv)
{
	timeStart = getTime();

	__djgpp_nearptr_enable();
	VGA += __djgpp_conventional_base;

	SFG_init();
	init_keyboard();
	set_mode(0x13);
	fill_rgb666_palette(paletteRGB565);
	set_unchained_mode();

	for (;;) {
		if (!SFG_mainLoopBody())
			break;

		draw_screen();
		page_flip(&visual_page, &active_page);
	}

	set_mode(0x03);
	done_keyboard();

	__djgpp_nearptr_disable();

	return 0;
}
