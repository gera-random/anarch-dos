#pragma once

void putc(char c)
{
	asm("int	$0x29\n"
	:
	: "al"(c));
}

void puts(const char *p_str)
{
	while (*p_str != 0) {
		if (*p_str == '\n') putc('\r');
		putc(*p_str);
		++p_str;
	}
}
