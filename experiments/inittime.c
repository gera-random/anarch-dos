#include <time.h>

#define SFG_FPS 60
#define SFG_LOG(str) puts(str);

#define SFG_SCREEN_RESOLUTION_X 320
#define SFG_SCREEN_RESOLUTION_Y 200

#include "../anarch/game.h"

uint8_t pixelBuffer[SFG_SCREEN_RESOLUTION_X][SFG_SCREEN_RESOLUTION_Y];
int pixels_drawn = 0;

void usleep(int32_t us)
{
	int16_t cx = us >> 8;
	int16_t dx = us & 0xFFFF;
	asm("mov	ah,	0x86\n"
		"mov	cx, [bp - 4]\n"
		"mov	dx, [bp - 8]\n"
		"int	0x15\n");
}

void getTime(int8_t *hour, int8_t *minutes, int8_t *seconds, int8_t *hundredths)
{
	int8_t local_h, local_m, local_s, local_ms;
	asm("mov	ah, 0x2C\n"
		"int	0x21\n"
		"mov	[bp - 4], ch\n"
		"mov	[bp - 8], cl\n"
		"mov	[bp - 12], dh\n"
		"mov	[bp - 16], dl\n");

	*hour = local_h;
	*minutes = local_m;
	*seconds = local_s;
	*hundredths = local_ms;
}

uint32_t SFG_getTimeMs() {
	int8_t h, m, s, ms;
	getTime(&h, &m, &s, &ms);
	return ms + s * 100 + m * 100 * 60 + h * 100 * 60 * 60;
}
void SFG_sleepMs(uint16_t timeMs) {
	usleep(timeMs * 1000);
}
void SFG_setPixel(uint16_t x, uint16_t y, uint8_t colorIndex) {
	pixelBuffer[x][y] = colorIndex;
	++pixels_drawn;
}
void SFG_processEvent(uint8_t event, uint8_t data) { }
int8_t SFG_keyPressed(uint8_t key) {
	return 0;
}
void SFG_getMouseOffset(int16_t *x, int16_t *y) { }
void SFG_setMusic(uint8_t value) { }
void SFG_playSound(uint8_t soundIndex, uint8_t volume) { }
uint8_t SFG_load(uint8_t data[SFG_SAVE_SIZE]) {
	return 0;
}
void SFG_save(uint8_t data[SFG_SAVE_SIZE]) { }

int main(void)
{
	uint32_t timeMs1 = SFG_getTimeMs();

	clock_t before_init = clock();


	SFG_init();

	clock_t after_init = clock();
	double time_init = (double)(after_init - before_init) / CLOCKS_PER_SEC;
	uint32_t timeMs2 = SFG_getTimeMs();
	uint32_t timeMs = timeMs2 - timeMs1;
	printf("%lf %d\n", time_init, timeMs);

	for (int i = 0; i < 1000; ++i) {
		SFG_mainLoopBody();
		if (i % 10 == 9) {
			clock_t after_loop = clock();
			double time_loop = (double)(after_loop - after_init) / CLOCKS_PER_SEC;
			printf("%lf %d\n", time_loop, pixels_drawn);
		}
	}

}
