// vim: noet sw=4 ts=4

#ifndef COMMON_H
#define COMMON_H

#define EOF -1

typedef int16_t FILE;
FILE one_handle = 0;
int8_t *VGA = (int8_t *)0xA0000;
int16_t visual_page = 0;
int16_t active_page = SCREEN_SIZE/4;

void putc(char c)
{
	asm("mov	al, [bp+8]\n"
		"int	0x29\n");
}

void put4bit(char digit)
{
	digit &= 0xF;
	if (digit < 10)
		putc('0' + digit);
	else
		putc('A' + digit - 10);
}
// printing byte values for debugging
void put8bit(char number)
{
	put4bit(number >> 4);
	put4bit(number);
}

void puts(const char *p_str)
{
	while (*p_str != 0) {
		// print \r before every \n
		if (*p_str == '\n')
			putc('\r');

		putc(*p_str);
		++p_str;
	}
}

char *strncpy(char *dest, const char *src, short n)
{
	short i = 0;
	for (; i < n && src[i] != '\0'; i++)
		dest[i] = src[i];
	for (; i < n; i++)
		dest[i] = '\0';

	return dest;
}

FILE *fcreate(const char *filename)
{
	int8_t carry = 1; // [bp - 4]
	FILE handle = one_handle; // [bp - 8]
	char filename_local[64]; // [bp - 72]
	strncpy(filename_local, filename, 64);

	asm("mov	ah, 0x3C\n"
		"mov	cx, 00000000b\n" // attributes 
		"lea	dx, [bp - 72]\n"
		"push	ss\n"
		"pop	ds\n"
		"int	0x21\n"
		"rcl	dx, 1\n" // save carry flag in dx bit 0
		"mov	[bp - 4], dx\n"
		"mov	[bp - 8], ax\n");
	carry &= 1;

	one_handle = handle;
	if (carry)
	{
		puts("fcreate: error\n");
		return 0;
	}
	return &one_handle;
}

FILE *fopen(const char *filename, const char *mode)
{
	int8_t dos_mode = 0; // [bp - 4]
	int8_t carry = 0; // [bp - 8]
	FILE handle = one_handle; // [bp - 12]
	char filename_local[64]; // [bp - 76]
	char err_handle_taken[] = "fopen: handle is already taken\n";

	strncpy(filename_local, filename, 64);
	while (*mode != 0) {
		if (*mode == 'w') dos_mode = 2;
		++mode;
	}
	
	if (handle != 0) {
		puts(err_handle_taken);
		return 0;
	}

	asm("mov	ah, 0x3D\n"
		"mov	al, [bp - 4]\n"
		"lea	dx, [bp - 76]\n"
		"push	ss\n"
		"pop	ds\n"
		"int	0x21\n"
		"rcl	dx, 1\n" // save carry flag in dx bit 0
		"mov	word [bp - 8], dx\n"
		"mov	[bp - 12], ax\n");
	carry &= 1;

	one_handle = handle;

	if (carry) {
		if (dos_mode > 0) one_handle = *fcreate(filename);
		else{ 
			one_handle = 0;
			return 0;
		}
	}
	
	return &one_handle;
}

int16_t fgetc(FILE *handle)
{
	FILE handle_local = *handle; // [bp - 4]
	int16_t num_read; // [bp - 8]
	int16_t read_buffer; // [bp - 12]
	int8_t carry; // [bp - 16]

	asm("mov	ah, 0x3F\n"
		"mov	cx, 1\n" // read 1 byte
		"mov	bx, [bp - 4]\n"
		"lea	dx, [bp - 12]\n"
		"push	ss\n"
		"pop	ds\n"
		"int	0x21\n"
		"mov	[bp - 8], ax\n"
		"rcl	dx, 1\n" // save carry flag in dx bit 0
		"mov	[bp - 16], dx\n");
	carry &= 1;

	if (carry | num_read != 1)
		return EOF;

	return (unsigned char)read_buffer;
}

int16_t fputc(unsigned char c, FILE *handle)
{
	FILE handle_local = *handle; // [bp - 4]
	int16_t num_written; // [bp - 8]
	asm("mov	ah, 0x40\n"
		"mov	cx, 1\n" // read 1 byte
		"mov	bx, [bp - 4]\n"
		"lea	dx, [bp + 8]\n" // function argument c
		"push	ss\n"
		"pop	ds\n"
		"int	0x21\n"
		"mov	[bp - 8], ax\n");

	if (num_written != 1) {
		return EOF;
	}

	return c;
}

int16_t fread(void *buffer, int16_t size, int16_t nmemb, FILE *handle)
{
	int16_t i = 0;
	while (i < nmemb) {
		int16_t j = 0;
		int16_t c;
		while (j < size) {
			c = fgetc(handle);
			if (c == EOF) {
				return i;
			}
			((char *)buffer)[size*i + j] = (char)c;
			++j;
		}
		++i;
	}
	return i;
}

int16_t fwrite(const void *buffer, int16_t size, int16_t nmemb, FILE *handle)
{
	int16_t i = 0;
	while (i < nmemb) {
		int16_t j = 0;
		int16_t c;
		while (j < size) {
			if (fputc(((int8_t *)buffer)[size*i + j], handle) == EOF) {
				return i;
			}
			++j;
		}
		++i;
	}
	return i;
}

int16_t fclose(FILE *handle)
{
	int8_t carry = 1; // [bp - 4]
	FILE handle_local = *handle; // [bp - 8]
	asm("mov	ah, 0x3E\n"
		"mov	bx, [bp - 8]\n"
		"int	0x21\n"
		"rcl	dx, 1\n" // save carry flag in dx bit 0
		"mov	[bp - 4], dx\n");
	carry &= 1;
	if (carry) return EOF;
	*handle = 0;
	return 0;
}

void getTime(int8_t *hour, int8_t *minutes, int8_t *seconds, int8_t *hundredths)
{
	int8_t local_h, local_m, local_s, local_ms;
	asm("mov	ah, 0x2C\n"
		"int	0x21\n"
		"mov	[bp - 4], ch\n"
		"mov	[bp - 8], cl\n"
		"mov	[bp - 12], dh\n"
		"mov	[bp - 16], dl\n");

	*hour = local_h;
	*minutes = local_m;
	*seconds = local_s;
	*hundredths = local_ms;
}

void select_plane(int8_t number)
{
	asm("mov	al, [bp + 8]\n"
		"mov	dx, 0x03c5\n"
		"out	dx, al\n");
}

void select_plane_mask(int16_t mask)
{
	asm("mov	ax, [bp + 8]\n"
		"mov	dx, 0x03c4\n"
		"out	dx, ax\n");
}

void set_unchained_mode(void)
{
	int32_t *ptr=(int32_t *)VGA;

	asm("mov	al, 0x04");
	asm("mov	dx, 0x03c4");
	asm("out	dx, al");

	asm("mov	al, 0x06");
	asm("mov	dx, 0x03c5");
	asm("out	dx, al");

	select_plane_mask(0xff02);
	
	for (int16_t i=0; i < 0x4000; ++i)
		*ptr++ = 0;

	asm("mov	al, 0x14");
	asm("mov	dx, 0x03d4");
	asm("out	dx, al");

	asm("mov	al, 0x00");
	asm("mov	dx, 0x03d5");
	asm("out	dx, al");

	asm("mov	al, 0x17");
	asm("mov	dx, 0x03d4");
	asm("out	dx, al");

	asm("mov	al, 0xe3");
	asm("mov	dx, 0x03d5");
	asm("out	dx, al");
}

void page_flip(int16_t *page1, int16_t *page2)
{
	int16_t high_address; // [bp - 4]
	int16_t low_address; // [bp - 8]
	int16_t temp;

	temp=*page1;
	*page1=*page2;
	*page2=temp;

	high_address = 0x0C | (*page1 & 0xff00);
	low_address  = 0x0D | (*page1 << 8);

	for (;;) {
		asm("mov	dx, 0x03da");
		asm("in		al, dx");
		asm("test	al, 0x08");
		asm("jnz	display_off");
	}
	asm("display_off:");

	asm("mov	ax, [bp - 4]");
	asm("mov	dx, 0x03d4");
	asm("out	dx, ax");

	asm("mov	ax, [bp - 8]");
	asm("mov	dx, 0x03d4");
	asm("out	dx, ax");

	for (;;) {
		asm("mov	dx, 0x03da");
		asm("in		al, dx");
		asm("test	al, 0x08");
		asm("jz		vretrace");
	}
	asm("vretrace:");
}

void clear()
{
	int32_t *ptr=(int32_t *)&VGA[active_page];
	select_plane_mask(0xff02);
	for (int16_t i=0; i < SCREEN_SIZE/4; ++i)
		*ptr++ = 0;

}

static void vga_on()
{
	asm("mov	ah, 0\n"
		"mov	al, 0x13\n" // 320x200 256-color graphics
		"int	0x10\n");
}

static void vga_off()
{
	asm("mov	ah, 0\n"
		"mov	al, 0x03\n" // default video mode
		"int	0x10\n");
}

void palette_start_input()
{
	asm("mov	al, 0\n"
		"mov	dx, 0x03c8\n"
		"out	dx, al\n");

}
// 256*3 colors must be added
void palette_add_color(int8_t color)
{
	asm("mov	al, [bp + 8]\n"
		"mov	dx, 0x03c9\n"
		"out	dx, al\n");
}

void drawPixel(int16_t x, int16_t y, int8_t color)
{
	select_plane_mask(0x02);

	/* if (y >= 0 && y < SFG_SCREEN_RESOLUTION_Y */
	/* 	&& x >= 0 && x < SFG_SCREEN_RESOLUTION_X) { */
		select_plane(1 << (x & 3));
		VGA[active_page + (y<<6) + (y<<4) + (x>>2)] = color;
	/* } */
}

void sleep(int32_t us)
{
	int16_t cx = us >> 8;
	int16_t dx = us & 0xFFFF;
	asm("mov	ah,	0x86\n"
		"mov	cx, [bp - 4]\n"
		"mov	dx, [bp - 8]\n"
		"int	0x15\n");
}

#endif
