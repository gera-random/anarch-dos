GCC=/usr/local/djgpp/bin/i586-pc-msdosdjgpp-gcc

anarch.exe: djgpp_main.c vga.h keyboard.h
	$(GCC) $< -o $@

.PHONY: clean
clean:
	-rm anarch.exe
