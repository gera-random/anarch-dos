// vim: noet sw=4 ts=4

#define SFG_SCREEN_RESOLUTION_X 320
#define SFG_SCREEN_RESOLUTION_Y 200
#define SCREEN_SIZE (SFG_SCREEN_RESOLUTION_X * SFG_SCREEN_RESOLUTION_Y)
#define SFG_FPS 22

#include "../anarch/game.h"
#include "smlrc_lib.h"

uint32_t SFG_getTimeMs() {
	int8_t h, m, s, ms;
	getTime(&h, &m, &s, &ms);
	return ms + s * 100 + m * 100 * 60 + h * 100 * 60 * 60;
}

void SFG_sleepMs(uint16_t timeMs) {
	sleep(timeMs * 1000);
}

void SFG_setPixel(uint16_t x, uint16_t y, uint8_t colorIndex)
{
	drawPixel(x, y, colorIndex);
}

void SFG_processEvent(uint8_t event, uint8_t data) { }
int8_t SFG_keyPressed(uint8_t key) {
	if (key == SFG_KEY_LEFT) return 1;
	return 0;
}
void SFG_getMouseOffset(int16_t *x, int16_t *y) { }
void SFG_setMusic(uint8_t value) { }
void SFG_playSound(uint8_t soundIndex, uint8_t volume) { }

void SFG_save(uint8_t data[SFG_SAVE_SIZE]) {
	FILE *f = fopen("anarch.sav","rwb");

	if (f == 0) {
		puts("SFG_save: cannot open save file\n");
	}
	else if (fwrite(data,1,SFG_SAVE_SIZE,f) != 1) {
		puts("SFG_save: partial write\n");
	}
	if (fclose(f) == EOF) {
		puts("SFG_save: file is not closed\n");
	}
}

uint8_t SFG_load(uint8_t data[SFG_SAVE_SIZE])
{
	FILE *f = fopen("anarch.sav","rb");

	puts("SFG_load: opening and reading save file\n");

	if (f == 0) {
		puts("SFG_load: no save file to open\n");
	}
	else if (fread(data,1,SFG_SAVE_SIZE,f) != 1) {
		puts("SFG_load: partial read\n");
	}
	if (fclose(f) == EOF) {
		puts("SFG_load: file is not closed\n");
	}
	return 1;
}

void fill_rgb666_palette(int16_t *rgb565_palette)
{
	palette_start_input();
	for (int i = 0; i < 256; ++i)
	{
		int8_t col = (rgb565_palette[i] >> 11) & 0x1f;
		col = col * 63 / 31;

		palette_add_color(col);

		col = (rgb565_palette[i] >> 5) & 0x3f;

		palette_add_color(col);

		col = rgb565_palette[i] & 0x1f;
		col = col * 63 / 31;

		palette_add_color(col);
	}
}

int main(void)
{
	SFG_init();

	vga_on();
	set_unchained_mode();

	fill_rgb666_palette(paletteRGB565);

	for(;;) {
		if (!SFG_mainLoopBody())
			break;

		page_flip(&visual_page, &active_page);
	}
	vga_off();

	return 0;
}
