#pragma once

#include <stdio.h>
#include <dpmi.h>
#include <stdint.h>
#include <go32.h>

uint8_t last_key;
uint8_t key_status[128];
_go32_dpmi_seginfo old_keyboard_handler, new_keyboard_handler;

// The new keyboard handler
void keyboard_handler(void)
{
	int8_t al, ah;
	asm("cli; pusha");

	// Read the key from the keyboard controller
	last_key=inportb(0x60);

	// Check if the key is pressed (bit 7 = 0) or released (bit 7 = 1)
	if(last_key<128) {
		key_status[last_key]=1;
	} else key_status[last_key-128]=0;

	// Tell to the controller that the key is readed
	al=inportb(0x61);
	al|=0x82;
	outportb(0x61,al);
	al&=0x7F;
	outportb(0x61,al);

	outportb(0x20, 0x20); // EOI
	asm("popa; sti");
}

// Install the new keyboard handler
void init_keyboard(void)
{
	register int i;

	last_key=0;
	for(i=0;i<128;i++)
		key_status[i]=0;

	new_keyboard_handler.pm_offset = (int)keyboard_handler;
	new_keyboard_handler.pm_selector = _go32_my_cs();
	_go32_dpmi_get_protected_mode_interrupt_vector(0x09, &old_keyboard_handler);
	_go32_dpmi_allocate_iret_wrapper(&new_keyboard_handler);
	_go32_dpmi_set_protected_mode_interrupt_vector(0x09, &new_keyboard_handler);
}

// Reinstall the old keyboard handler
void done_keyboard(void)
{
	_go32_dpmi_set_protected_mode_interrupt_vector(0x09, &old_keyboard_handler);
	_go32_dpmi_free_iret_wrapper(&new_keyboard_handler);
}
