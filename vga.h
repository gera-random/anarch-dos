#pragma once

#include <dos.h>
#include <stdint.h>

#define VIDEO_INT           0x10      /* the BIOS video interrupt. */
#define SET_MODE            0x00      /* BIOS func to set the video mode. */

#define SC_INDEX            0x03c4    /* VGA sequence controller */
#define SC_DATA             0x03c5
#define PALETTE_INDEX       0x03c8    /* VGA digital-to-analog converter */
#define PALETTE_DATA        0x03c9
#define CRTC_INDEX          0x03d4    /* VGA CRT controller */
#define CRTC_DATA           0x03d5
#define INPUT_STATUS_1      0x03da

#define MAP_MASK            0x02      /* Sequence controller registers */
#define ALL_PLANES          0xff02
#define MEMORY_MODE         0x04

#define HIGH_ADDRESS        0x0C      /* CRT controller registers */
#define LOW_ADDRESS         0x0D
#define UNDERLINE_LOCATION  0x14
#define MODE_CONTROL        0x17

#define SCREEN_WIDTH        320
#define SCREEN_HEIGHT       200
#define SCREEN_SIZE         (uint16_t)(SCREEN_WIDTH*SCREEN_HEIGHT)

#define DISPLAY_ENABLE      0x01      /* VGA input status bits */
#define VRETRACE            0x08

int8_t *VGA = (int8_t *)0xA0000;          /* this points to video memory. */
int16_t visual_page = 0;
int16_t active_page = SCREEN_SIZE/4;

int8_t screen[SCREEN_SIZE];

void set_mode(int8_t mode)
{
	union REGS regs;

	regs.h.ah = SET_MODE;
	regs.h.al = mode;
	int86(VIDEO_INT, &regs, &regs);
}

void set_unchained_mode(void)
{
	int16_t i;
	int32_t *ptr=(int32_t *)VGA;            /* used for faster screen clearing */

	outp(SC_INDEX,  MEMORY_MODE);       /* turn off chain-4 mode */
	outp(SC_DATA,   0x06);

	outpw(SC_INDEX, ALL_PLANES);        /* set map mask to all 4 planes */

	for(i=0; i<0x4000; i++)               /* clear all 256K of memory */
		*ptr++ = 0;

	outp(CRTC_INDEX, UNDERLINE_LOCATION);/* turn off long mode */
	outp(CRTC_DATA, 0x00);

	outp(CRTC_INDEX, MODE_CONTROL);      /* turn on int8_t mode */
	outp(CRTC_DATA, 0xe3);
}

void page_flip(int16_t *page1, int16_t *page2)
{
	int16_t high_address,low_address;
	int16_t temp;

	temp=*page1;
	*page1=*page2;
	*page2=temp;

	high_address = HIGH_ADDRESS | (*page1 & 0xff00);
	low_address  = LOW_ADDRESS  | (*page1 << 8);

	#ifdef VERTICAL_RETRACE
	while ((inp(INPUT_STATUS_1) & DISPLAY_ENABLE));
	#endif
	outpw(CRTC_INDEX, high_address);
	outpw(CRTC_INDEX, low_address);
	#ifdef VERTICAL_RETRACE
	while (!(inp(INPUT_STATUS_1) & VRETRACE));
	#endif
}

void plot_pixel(int x, int y, int8_t color)
{
	screen[y*SCREEN_WIDTH + x] = color;
}

void draw_screen()
{
	outp(SC_INDEX, MAP_MASK);
	for (int plane = 0; plane < 4; ++plane) {
		outp(SC_DATA, 1 << plane);
		for (int x = plane; x < SCREEN_WIDTH; x+= 4) {
			for (int y = 0; y < SCREEN_SIZE; y += SCREEN_WIDTH) {
				VGA[active_page + ((y + x) >> 2)] = screen[y+x];
			}
		}
	}
}

void palette_start_input()
{
	outp(PALETTE_INDEX, 0);
}

void palette_add_color(int8_t color)
{
	outp(PALETTE_DATA, color);
}
