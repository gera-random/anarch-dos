#include <stdint.h>
#include <stdio.h>

#define RESOLUTION_X 320
#define RESOLUTION_Y 200
#define SCREEN_SIZE (RESOLUTION_X*RESOLUTION_Y)

int8_t *VGA = (int8_t *)0xA0000;
int16_t visual_page = 0;
int16_t active_page = SCREEN_SIZE/4;

static void vga_on()
{
	asm("mov	ah, 0\n"
		"mov	al, 0x13\n" // 320x200 256-color graphics
		"int	0x10\n");
}

static void vga_off()
{
	asm("mov	ah, 0\n"
		"mov	al, 0x03\n" // default video mode
		"int	0x10\n");
}

void select_plane(int8_t number)
{
	asm("mov	al, [bp + 8]\n"
		"mov	dx, 0x03c5\n"
		"out	dx, al\n");
}

void select_plane_mask(int16_t mask)
{
	asm("mov	ax, [bp + 8]\n"
		"mov	dx, 0x03c4\n"
		"out	dx, ax\n");
}

void set_unchained_mode(void)
{
	int32_t *ptr=(int32_t *)VGA;

	asm("mov	al, 0x04");
	asm("mov	dx, 0x03c4");
	asm("out	dx, al");

	asm("mov	al, 0x06");
	asm("mov	dx, 0x03c5");
	asm("out	dx, al");

	select_plane_mask(0xff02);
	
	for (int16_t i=0; i < 0x4000; ++i)
		*ptr++ = 0;

	asm("mov	al, 0x14");
	asm("mov	dx, 0x03d4");
	asm("out	dx, al");

	asm("mov	al, 0x00");
	asm("mov	dx, 0x03d5");
	asm("out	dx, al");

	asm("mov	al, 0x17");
	asm("mov	dx, 0x03d4");
	asm("out	dx, al");

	asm("mov	al, 0xe3");
	asm("mov	dx, 0x03d5");
	asm("out	dx, al");
}


void page_flip(int16_t *page1, int16_t *page2)
{
	int16_t high_address; // [bp - 4]
	int16_t low_address; // [bp - 8]
	int16_t temp;

	temp=*page1;
	*page1=*page2;
	*page2=temp;

	high_address = 0x0C | (*page1 & 0xff00);
	low_address  = 0x0D | (*page1 << 8);

	for (;;) {
		asm("mov	dx, 0x03da");
		asm("in		al, dx");
		asm("test	al, 0x08");
		asm("jnz	display_off");
	}
	asm("display_off:");

	asm("mov	ax, [bp - 4]");
	asm("mov	dx, 0x03d4");
	asm("out	dx, ax");

	asm("mov	ax, [bp - 8]");
	asm("mov	dx, 0x03d4");
	asm("out	dx, ax");

	for (;;) {
		asm("mov	dx, 0x03da");
		asm("in		al, dx");
		asm("test	al, 0x08");
		asm("jz		vretrace");
	}
	asm("vretrace:");
}


void drawPixel(int16_t x, int16_t y, int8_t color)
{
	select_plane_mask(0x02);
	select_plane(1 << (x & 3));
	VGA[active_page + (y<<6) + (y<<4) + (x>>2)] = color;
}


int main(void)
{
	vga_on();
	set_unchained_mode();

	uint8_t c = 0;
	for(;;c = (c+1)%0xFF) {
		for (uint8_t x = 0; x < 200; ++x)
			for (uint8_t y = 0; y < 200; ++y)
				drawPixel(x, y, c);

		page_flip(&visual_page, &active_page);
	}

	vga_off();

	return 0;
}
