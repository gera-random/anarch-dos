asm(".code16gcc\n"
    "call	dosmain\n"
    "mov	$0x4C, %ah\n"
    "int	$0x21\n");

#include "common.h"

void farpokeb(short seg, short off, char v)
{
	asm("mov  %0, %%fs\n\t"
	    "movb %2, %%fs:(%1)\n\t"
	:
	: "g"(seg), "b"(off), "r"(v));
}

void load_bin(char *filename)
{
	short handle;
	char carry;
	asm("mov	$0x3D, %%ah\n"
	    "mov	$0,%%al\n"
		"int	$0x21\n"
	: "=a"(handle), "=@ccc"(carry)
	: "d"(filename));
	if (carry) {
		puts("load_bin: cannot open file\n");
		return;
	}

	// place the binary at 0x7E00
	short segment = 0x7E0;
	short offset = 0x100;

	short num_read, read_buffer;
	for(;;) {
		asm("mov	$0x3F, %%ah\n"
		    "mov	$1, %%cx\n"
		    "int	$0x21\n"
		: "=a"(num_read), "=@ccc"(carry)
		: "b"(handle), "d"(&read_buffer));

		if (num_read != 1) break;

		farpokeb(segment, offset, read_buffer);
		++offset;
		if (offset >= 16) {segment += offset/16; offset %= 16;}
	}

	asm("mov	$0x3E, %%ah\n"
	    "mov	$0,%%al\n"
	    "int	$0x21\n"
	: "=@ccc"(carry)
	: "b"(handle));
	if (carry) {
		puts("load_bin: cannot close file\n");
	}
	return;
}

void jump_to_bin()
{
	asm("mov	$0x7E0, %ax\n"
		"mov	%ax, %ds\n"
		"mov	%ax, %es\n"
		"mov	%ax, %sp\n"
		"mov	%ax, %ss\n"
		"jmp	$0x7E0,$0x100\n");
}

int dosmain(void)
{
	load_bin("other16.bin");
	jump_to_bin();

	return 0;
}
